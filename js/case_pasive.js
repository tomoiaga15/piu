function meaning() {
  var x = document.getElementById("infoArea");
  x.value = "Passive houses are characterized, first of all, by a high thermal comfort and a high quality of indoor air. High thermal comfort means comfortable and constant temperatures throughout the building, without unpleasant air currents, without the risk of condensation or dampness.\r\nThe superior air quality is given by an innovative ventilation system with heat recovery system, which permanently introduces fresh air into the building at a comfortable temperature. The vitiated air from the high humidity and odor areas, such as the kitchen and the bathrooms, is extracted and evacuated from the building, not before it gives off heat, to a large percentage, to the fresh air entering the building.\r\nA passive building, ventilated by such a system, permanently benefits from fresh air, with minimum concentrations of CO2 and formaldehyde, respectively volatile chemical compounds. In addition to this advantage, the filters installed in the ventilation unit do not allow dust, allergens, pollutants to enter inside.\r\nA second representative feature is the very high energy efficiency. The PH standard is currently the standard for the highest energy-efficient buildings in the world. The energy consumption for heating a passive house is lower by more than 75% compared to the similar consumption of a conventional house.";
}

function vs() {
  var x = document.getElementById("infoArea");
  x.value = "The passive house must meet extremely demanding requirements regarding the energy consumption for heating and cooling, the primary energy requirement or the air tightness of the tire.\r\nIf all the criteria required by the PH Standard are met, that house can obtain passive house certification. Instead, the concepts of eco house, green house or low energy house are very vague, interpretable concepts. There is no institution that imposes certain requirements, at least minimal, so that a building can be included in one of these categories.";
}

function certification() {
  var x = document.getElementById("infoArea");
  x.value = "Passivhaus Institute in Darmstadt, Germany, along with world-class accredited building certification. There is also the possibility of certification in Romania, through a passive buildings certifier accredited by PHI (Passivhaus Institut).\r\nThis standard (PH) refers to multi-function buildings, from individual, collective housing to office buildings, schools, kindergartens, indoor pools, supermarkets, hospitals, etc.";
}

function needs() {
  var x = document.getElementById("infoArea");
  x.value = "A passive house can be built from any material, as long as the design principles are respected. What is different, most of the times, are the details of the execution and the way of putting it into opera. Without a very good quality of execution, we cannot achieve the desire of passive house.";
}

function costs() {
  var x = document.getElementById("infoArea");
  x.value = "There are many preconceived ideas, including that a passive house is twice or three times more expensive than a conventional house.\r\nFor the construction of a passive house extra-investment in materials with higher performance does not exceed 10-15% compared to the investment in a conventional building, designed according to the national norms in force.\r\nAs for larger constructions and with other functions, the extra investment is about 5-8% higher, compared to a conventional construction.\r\nThese additional costs are recovered in 5-10 years of use of that building, precisely through the low energy consumption, and then, profit.\r\nIn the case of a conventional construction, the energy costs, which are never estimated or optimized even from the design stage, surprise the beneficiaries, being about 8-10 times higher than those for a passive house.";
}