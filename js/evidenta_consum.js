window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	axisX:{
		title: "Week",
		includeZero: true,
		interval: 1
	},
	axisY:{
		title: "Average Consume",
		suffix: "kW",
		includeZero: true
	},
	data: [{   
		yValueFormatString: "#### kW",
		type: "line",       
		dataPoints: [
			{x: 1, y: 2700 },
			{x: 2, y: 4700 },
			{x: 3, y: 3700 },
			{x: 4, y: 5700 },
			{x: 5, y: 13700 },
			{x: 6, y: 52700 }
		]
	}]
});
chart.render();

}