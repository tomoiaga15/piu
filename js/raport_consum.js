function refresh() {
	var table = document.getElementById("devTable");
	var modal = document.getElementById("myModal");
	var error = document.getElementById("error");
	// for (var i = 0, row; row = table.rows[i]; i++) {
	//    //iterate through rows
	//    //rows would be accessed using the "row" variable assigned in the for loop
	//    for (var j = 0, col; col = row.cells[j]; j++) {
	//      //iterate through columns
	//      //columns would be accessed using the "col" variable assigned in the for loop
	//    }  
	// }
	var ul = document.getElementById("list");
	ul.innerHTML = "";
	var checkBoxes = table.getElementsByTagName("input");
	console.log(checkBoxes);
	for (var i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
		  	var li = document.createElement("li");
		  	if(checkBoxes[i+1].value === ""){
				modal.style.display = "block";
		  		error.innerHTML = "Error! You forgot to write the hours value for " + checkBoxes[i].value;
		  		return;
		  	}else if(isNaN(checkBoxes[i+1].value)){
		  		modal.style.display = "block";
		  		error.innerHTML = "Error! The following is not a number : " + checkBoxes[i+1].value;
		  		return;
		  	}else{
			  	li.appendChild(document.createTextNode(checkBoxes[i].value + " - " + checkBoxes[i+1].value +" hours"));
			  	ul.appendChild(li);  
		  	}  	
        }
    }
}

function addDevice() {
	var table = document.getElementById("devTable");
	var newRow = document.getElementById("newDev");
	if(newRow.value !== ""){
		var row = table.insertRow(1);
	  	var cell1 = row.insertCell(0);
	  	var cell2 = row.insertCell(1);
	  	var cell3 = row.insertCell(2);
	  	cell1.innerHTML = "<input class='checkBox' type='checkbox' name='" + newRow.value + "' value='" + newRow.value + "' />";
	  	cell2.innerHTML = newRow.value;
	  	cell3.innerHTML = "<input class='consume' name='consume'>";
  	}else{
		var modal = document.getElementById("myModal");
		  modal.style.display = "block";
		  var error = document.getElementById("error");
		  error.innerHTML = "Invalid device!";
	}
}

window.onclick = function(event) {
  var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function spanClose() {
  var modal = document.getElementById("myModal");
  modal.style.display = "none";
}

function submit(){
  var modal = document.getElementById("myModal");
  modal.style.display = "block";
  var error = document.getElementById("error");
  error.innerHTML = "Report submitted! Our specialists will send you a response soon! Thank you for using our app.";
}

function getReport() {
  location.replace("evidenta_consum.html");
}