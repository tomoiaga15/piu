
function showSliderValue(){
	var slider=document.getElementById("myRange");
    var outp=document.getElementById("outVal");
	var saveB=document.getElementById("sb");
	var modal = document.getElementById("myModal");
     modal.style.display = "none";
	saveB.addEventListener("click", saveChanges);
    outp.innerHTML=slider.value+ "%";
    
   slider.oninput=function(){
	  outp.innerHTML=  this.value + "%";
   }
   
   var resetB=document.getElementById("resetb");
	resetB.addEventListener("click", resetChanges);
   
	
}


function resetChanges(){
 document.getElementById("s1").checked=false;
	document.getElementById("s2").checked=false;
	document.getElementById("s3").checked=false;
	document.getElementById("s4").checked=false;
	document.getElementById("s5").checked=false;

	document.getElementById("rb1").checked=false;
	document.getElementById("rb2").checked=false;

	var slider=document.getElementById("myRange");
	slider.value="0";
	
	var outp=document.getElementById("outVal");
	outp.innerHTML=0 + "%";

}

function saveChanges(){

	var checkBoxes = document.getElementsByName("spots");
	var outp=document.getElementById("outSave");
	console.log(checkBoxes);
	var count=0;
	for (var i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
			count += 1;
        }
    }
	var radiob = document.getElementsByClassName("rb");
	var count1=0;
	for (var i = 0; i < radiob.length; i++) {
        if (radiob[i].checked) {
			count1 += 1;
        }
    }
	
	if (count==0){
		outp.innerHTML="You have to select one light source";
		outp.style.display="block";
	} else if(count1==0){
		outp.innerHTML="You have to select one type of light";
		outp.style.display="block";
	} else if(document.getElementById("myRange").value==0)
	{
    	outp.innerHTML="You have to select light intensity";
		outp.style.display="block";	
	}
	else{
		outp.innerHTML="";
		outp.style.display="none";	
		submitChanges();
	}
}



function submitChanges(){
  var modal = document.getElementById("myModal");
  modal.style.display = "block";
  var error = document.getElementById("error");
  error.innerHTML = "Settings saved successfully.";
}



function spanClose() {
  var modal = document.getElementById("myModal");
  modal.style.display = "none";
  resetChanges();
}




window.onload = showSliderValue;