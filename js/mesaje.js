function change() {
  var selected = document.getElementById("inbox").value;
  var area = document.getElementById("messageArea");

  if(selected === 'news'){
    area.value = "Better homes with less energy\r\n\r\n" +
                  "Passive House Conference in Berlin aims for a sustainable future – September 2020" +
                  "Darmstadt, Germany. Saving energy is comparatively easy in the case of buildings:" +
                  "these can be built so that they will not use much energy in the first place. If renewable" +
                  "energy is added to the mix, then sustainability is optimally implemented: “Building the" +
                  "future – sustainably!, this is the focal theme of the 24th International Passive House" +
                  "Conference 2020 in Berlin. The German Federal Ministry of Economics has assumed" +
                  "patronage for the conference. The call for papers has begun.\r\n" +
                  "Energy efficiency comes first! In order to do more for climate protection in the building sector, first," +
                  "the efficiency of buildings must be improved significantly. Buildings will then require very little" +
                  "energy for heating or any cooling. This applies equally for new buildings and retrofits. After that," +
                  "the energy efficient buildings can be supplied" +
                  "entirely with regionally produced renewable" +
                  "energy. A big advantage of highly efficient" +
                  "buildings is their perceptibly higher level of" +
                  "living comfort. The 24th International Passive" +
                  "House Conference, to which the Passive" +
                  "House Institute and its partners are inviting" +
                  "everyone on 20 and 21 September 2020" +
                  "(Sunday and Monday) at the Estrel Congress" +
                  "Center in Berlin, will focus on Building the" +
                  "future – sustainably!“\r\n\r\n" +
                  "Green Deal\r\n\r\n" +
                  "The recently introduced European Green Deal of the European Commission also mentions highly" +
                  "energy efficient buildings as a contribution to more effective climate protection. For this purpose," +
                  "among other things, the number of building modernisations will be increase substantially, in that" +
                  "the EU and its member" +
                  "states take part in a wave" +
                  "of modernisations. With" +
                  "the Green Deal, the" +
                  "European economies will" +
                  "become independent of" +
                  "fossil fuels and climate" +
                  "neutral by the year 2050.";
  }
  if(selected === 'summary'){
    area.value = "Thank you for your order!\r\n\r\n Here is the order summary:\r\n  1 X Solar Panel WT 300M . . . . . . . . . . . . . . . . . . 200 EUR\r\n"  +     
                                                                                 "                                                                       Total: 200 EUR";
  }
  if(selected === 'place'){
    area.value = "Thank you for your order!\r\n\r\nYour order has been placed! You will receive an order summary.";
  }
  if(selected === 'welcome'){
    area.value = "Welcome!\r\n\r\nThank you for using our servicies, you will not regret it.";
  }
}