function change() {
  var selected = document.getElementById("inbox").value;
  var area = document.getElementById("messageArea");

  if(selected === '1st'){
    area.value = "How can you buy solar panels?\r\n\r\n" +
                  "After you login, you can choose the 'Buy' options, which will redirect you to the Suppliers page, where you can choose a supplier and after choose the solar panels you want to buy.";
  }
  if(selected === '2nd'){
    area.value = "Can you control the solar panels through this app?\r\n\r\nYes! You can do this by accessing the ENERGY SOURCES MANAGEMENT option in the main screen. There you can select the parameters for every panel you want.";
  }
  if(selected === '3rd'){
    area.value = "How can I get a Passive House Certification?\r\n\r\nYou can find all the information regarding Passive Houses in the main screen, by accessing the Passive Houses option.";
  }
}