function refresh() {
	var table = document.getElementById("mainDiv");
	var modal = document.getElementById("myModal");
	var error = document.getElementById("error");
	var count = 0;
	var checkBoxes = table.getElementsByTagName("input");
	console.log(checkBoxes);
	for (var i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
			count += 1;
        }
    }
	if(count > 4) {
		modal.style.display = "block";
		error.innerHTML = "Error! You checked too many boxes";
		return;
	}
	if(count < 4) {
		modal.style.display = "block";
		error.innerHTML = "Error! You have to check one box for every question";
		return;
	}
	if(checkBoxes[0].value ===""){
		modal.style.display = "block";
		error.innerHTML = "Error! You forgot to answer question 1";
		return;
	}
	if(checkBoxes[3].value ===""){
		modal.style.display = "block";
		error.innerHTML = "Error! You forgot to answer question 3";
		return;
	}
	submit();
}

window.onclick = function(event) {
  var modal = document.getElementById("myModal");
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

function spanClose() {
  var modal = document.getElementById("myModal");
  modal.style.display = "none";
}

function submit(){
  var modal = document.getElementById("myModal");
  modal.style.display = "block";
  var error = document.getElementById("error");
  error.innerHTML = "Report submitted! Our specialists will send you a response soon! Thank you for using our app.";
}