window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	axisX:{
		title: "Device",
		includeZero: true,
		interval: 1
	},
	axisY:{
		title: "Test Result",
		suffix: "kW",
		includeZero: true
	},
	data: [{   
		yValueFormatString: "#### kW",
		type: "line",       
		dataPoints: [
			{x: 1, y: 75 },
			{x: 2, y: 23 },
			{x: 3, y: 63 },
			{x: 4, y: 95 },
			{x: 5, y: 87 },
			{x: 6, y: 26 }
		]
	}]
});
chart.render();

}

function myFunction(){
	var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2",
	axisX:{
		title: "Device",
		includeZero: true,
		interval: 1
	},
	axisY:{
		title: "Test Result",
		suffix: "kW",
		includeZero: true
	},
	data: [{   
		yValueFormatString: "#### kW",
		type: "line",       
		dataPoints: [
			{x: 1, y: Math.floor(Math.random() * 100) },
			{x: 2, y: Math.floor(Math.random() * 100) },
			{x: 3, y: Math.floor(Math.random() * 100) },
			{x: 4, y: Math.floor(Math.random() * 100) },
			{x: 5, y: Math.floor(Math.random() * 100) },
			{x: 6, y: Math.floor(Math.random() * 100) }
		]
	}]
});
chart.render();
}


function spanClose() {
  var modal = document.getElementById("myModal");
  modal.style.display = "none";
}

function submit(){
  var modal = document.getElementById("myModal");
  modal.style.display = "block";
  var error = document.getElementById("error");
  error.innerHTML = "Report submitted! The test will start with first selected date.";
}